SUMMARY = "Analyzer service"
LICENSE = "CLOSED"

inherit cmake pkgconfig systemd

DEPENDS = "glib-2.0 dlt-daemon"

SRCREV = "af9c02f3e8c96bb00d6e48fe283c67f58cd5f0e8"
PR = "r0"

PROTOCOL = ";protocol=ssh"
BRANCH = ";branch=main"

SRC_URI = "git://gitlab.com/owl-os/analyzer-service.git${PROTOCOL}${BRANCH}"

S = "${WORKDIR}/git"

FILES:${PN} += "${systemd_system_unitdir}/*"
SYSTEMD_SERVICE:${PN} = "AnalyzerService.service"
SYSTEMD_AUTO_ENABLE = "enable"