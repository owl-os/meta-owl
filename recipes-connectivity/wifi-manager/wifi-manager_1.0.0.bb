SUMMARY = "Wifi manager API"
LICENSE = "CLOSED"

inherit cmake pkgconfig systemd

DEPENDS = "glib-2.0 dlt-daemon networkmanager"

SRCREV = "ca6558b5478185e460ede995d58ad52aebdf5683"
PR = "r0"

PROTOCOL = ";protocol=ssh"
BRANCH = ";branch=main"

FILESPATH =+ "${THISDIR}:"
SRC_URI = "file://${PN}"
S = "${WORKDIR}/${PN}"

#SRC_URI = "git://gitlab.com/owl-os/wifi-manager.git${PROTOCOL}${BRANCH}"

#S = "${WORKDIR}/git"

FILES:${PN} += "${systemd_system_unitdir}/*"
SYSTEMD_SERVICE:${PN} = "WifiManager.service"
SYSTEMD_AUTO_ENABLE = "enable"