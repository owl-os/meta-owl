DESCRIPTION = "qt5 wayland components from meta-qt5 layer"
LICENSE = "CLOSED"

PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = " \
    qtwayland-plugins \
"
