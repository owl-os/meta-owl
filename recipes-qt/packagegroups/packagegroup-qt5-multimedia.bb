DESCRIPTION = "qt5 multimedia components from meta-qt5 layer"
LICENSE = "CLOSED"

PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = " \
    qtmultimedia-plugins \
    qtmultimedia-qmlplugins \
"