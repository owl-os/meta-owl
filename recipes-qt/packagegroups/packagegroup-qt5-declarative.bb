DESCRIPTION = "qt5 declarative components from meta-qt5 layer"
LICENSE = "CLOSED"

PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = " \
    qtdeclarative-qmlplugins \
"