DESCRIPTION = "qt5 qtgraphicaleffects components from meta-qt5 layer"
LICENSE = "CLOSED"

PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = " \
    qtgraphicaleffects-qmlplugins \
"