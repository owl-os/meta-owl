DESCRIPTION = "qt5 qtquickcontrols components from meta-qt5 layer"
LICENSE = "CLOSED"
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = " \
    qtquickcontrols-dev \
    qtquickcontrols2-dev \
"
