DESCRIPTION = "qt5 core components from meta-qt5 layer"
LICENSE = "CLOSED"
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = " \
    qtbase-plugins \
"
