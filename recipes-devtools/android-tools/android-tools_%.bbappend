FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://usb-debugging-enabled"

FILES:${PN}-adbd += "${localstatedir}/usb-debugging-enabled"

do_install:append() {
   install -d ${D}${localstatedir}
   install -D -m 0600 ${WORKDIR}/usb-debugging-enabled ${D}${localstatedir}/usb-debugging-enabled
}