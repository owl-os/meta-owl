inherit core-image

SUMMARY = "A very basic Owl core image with Wayland"

IMAGE_FEATURES += "splash package-management ssh-server-dropbear hwcodecs weston"

CORE_IMAGE_EXTRA_INSTALL += " \
    linux-firmware-rpidistro-bcm43456 \
    weston  \
    wayland \
    bluez5 \
    networkmanager \
    openssh-sftp-server \
    rsync \
    dlt-daemon \
    android-tools \
    android-tools-adbd \
"

IMAGE_INSTALL:append = " \
    packagegroup-qt5-core \
    packagegroup-qt5-declarative \
    packagegroup-qt5-graphicaleffects \
    packagegroup-qt5-multimedia \
    packagegroup-qt5-qtquickcontrols \
    packagegroup-qt5-wayland \
    packagegroup-owl-core \
"

IMAGE_FSTYPES = "rpi-sdimg"