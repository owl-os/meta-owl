SUMMARY = "Owl os components"
DESCRIPTION = "Packages used by all distro variants of Owl"
LICENSE = "CLOSED"

# You don't need to change this value when you're changing just a RDEPENDS_${PN}
# or PACKAGESET variable.
PR = "r0"

inherit packagegroup

# - Add top-level packages to install in the images for all targets here.
RDEPENDS:${PN} = " \
    analyzer-service \
"